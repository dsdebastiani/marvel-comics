package org.bitbucket.dsdebastiani.marvelcomics.repository.db

import android.arch.paging.DataSource
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import org.bitbucket.dsdebastiani.marvelcomics.entities.CharacterEntity

@Dao
interface CharacterDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(posts : List<CharacterEntity>)

    @Query("SELECT * FROM characters ORDER BY indexInResponse ASC")
    fun characters() : DataSource.Factory<Int, CharacterEntity>

    @Query("DELETE FROM characters")
    fun deleteAll()

}