package org.bitbucket.dsdebastiani.marvelcomics.ui

import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import org.bitbucket.dsdebastiani.marvelcomics.GlideRequests
import org.bitbucket.dsdebastiani.marvelcomics.R
import org.bitbucket.dsdebastiani.marvelcomics.entities.CharacterEntity

class CharacterViewHolder(view: View, private val glide: GlideRequests) : RecyclerView.ViewHolder(view) {
    private val title: TextView = view.findViewById(R.id.title)
    private val subtitle: TextView = view.findViewById(R.id.subtitle)
    private val thumbnail : ImageView = view.findViewById(R.id.thumbnail)
    private var charactrer : CharacterEntity? = null
    init {
        view.setOnClickListener {
            charactrer?.thumbnail?.let { url ->
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                view.context.startActivity(intent)
            }
        }
    }

    fun bind(character: CharacterEntity?) {
        this.charactrer = character
        title.text = character?.name ?: "loading"
        subtitle.text = itemView.context.resources.getString(R.string.post_subtitle,
                character?.description ?: "")
        if (character?.thumbnail?.startsWith("http") == true) {
            thumbnail.visibility = View.VISIBLE
            glide.load(character.thumbnail)
                    .centerCrop()
                    .placeholder(R.drawable.ic_photo_black_48dp)
                    .into(thumbnail)
        } else {
            thumbnail.visibility = View.GONE
            glide.clear(thumbnail)
        }
    }

    companion object {
        fun create(parent: ViewGroup, glide: GlideRequests): CharacterViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.marvel_character_item, parent, false)
            return CharacterViewHolder(view, glide)
        }
    }
}