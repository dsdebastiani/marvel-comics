package org.bitbucket.dsdebastiani.marvelcomics.repository.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import org.bitbucket.dsdebastiani.marvelcomics.entities.CharacterEntity

@Database(
        entities = [(CharacterEntity::class)],
        version = 1,
        exportSchema = false
)
abstract class MarvelDb : RoomDatabase() {
    companion object {
        fun create(context: Context): MarvelDb {
            val databaseBuilder = Room.databaseBuilder(
                    context, MarvelDb::class.java,
                    "marvel_comics.db"
            )
            return databaseBuilder
                    .fallbackToDestructiveMigration()
                    .build()
        }
    }

    abstract fun characters(): CharacterDao
}