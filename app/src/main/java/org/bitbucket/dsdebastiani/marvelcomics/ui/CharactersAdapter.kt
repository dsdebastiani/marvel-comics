package org.bitbucket.dsdebastiani.marvelcomics.ui

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import org.bitbucket.dsdebastiani.marvelcomics.GlideRequests
import org.bitbucket.dsdebastiani.marvelcomics.R
import org.bitbucket.dsdebastiani.marvelcomics.repository.NetworkState
import org.bitbucket.dsdebastiani.marvelcomics.entities.CharacterEntity

class CharactersAdapter(private val glide: GlideRequests, private val retryCallback: () -> Unit)
    : PagedListAdapter<CharacterEntity, RecyclerView.ViewHolder>(POST_COMPARATOR) {

    private var networkState: NetworkState? = null

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.marvel_character_item -> (holder as CharacterViewHolder).bind(getItem(position))
            R.layout.network_state_item -> (holder as NetworkStateItemViewHolder).bindTo(networkState)
        }
    }

    override fun onBindViewHolder(
            holder: RecyclerView.ViewHolder,
            position: Int,
            payloads: MutableList<Any>) {
        if (payloads.isNotEmpty()) {
        } else {
            onBindViewHolder(holder, position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.marvel_character_item -> CharacterViewHolder.create(parent, glide)
            R.layout.network_state_item -> NetworkStateItemViewHolder.create(parent, retryCallback)
            else -> throw IllegalArgumentException("unknown view type $viewType")
        }
    }

    private fun hasExtraRow() = networkState != null && networkState != NetworkState.LOADED

    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position == itemCount - 1) {
            R.layout.network_state_item
        } else {
            R.layout.marvel_character_item
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasExtraRow()) 1 else 0
    }

    fun setNetworkState(newNetworkState: NetworkState?) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount())
            } else {
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }

    companion object {
        private val PAYLOAD = Any()
        val POST_COMPARATOR = object : DiffUtil.ItemCallback<CharacterEntity>() {
            override fun areContentsTheSame(oldItem: CharacterEntity, newItem: CharacterEntity): Boolean =
                    oldItem == newItem

            override fun areItemsTheSame(oldItem: CharacterEntity, newItem: CharacterEntity): Boolean =
                    oldItem.id == newItem.id

            override fun getChangePayload(oldItem: CharacterEntity, newItem: CharacterEntity): Any? {
                return if (sameExceptScore(oldItem, newItem)) {
                    PAYLOAD
                } else {
                    null
                }
            }
        }

        private fun sameExceptScore(oldItem: CharacterEntity, newItem: CharacterEntity): Boolean {
            // dummy :P
            return oldItem.copy() == newItem
        }
    }
}
