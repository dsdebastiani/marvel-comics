package org.bitbucket.dsdebastiani.marvelcomics.repository

import android.arch.paging.PagedList
import android.arch.paging.PagingRequestHelper
import android.support.annotation.MainThread
import org.bitbucket.dsdebastiani.marvelcomics.api.MarvelApi
import org.bitbucket.dsdebastiani.marvelcomics.entities.CharacterEntity
import org.bitbucket.dsdebastiani.marvelcomics.util.createStatusLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.Executor

class CharacterBoundaryCallback(
        private val webservice: MarvelApi,
        private val handleResponse: (MarvelApi.DataWrapper<MarvelApi.CharacterResponse>?) -> Unit,
        private val ioExecutor: Executor,
        private val networkPageSize: Int)
    : PagedList.BoundaryCallback<CharacterEntity>() {

    val helper = PagingRequestHelper(ioExecutor)
    val networkState = helper.createStatusLiveData()

    @MainThread
    override fun onZeroItemsLoaded() {
        helper.runIfNotRunning(PagingRequestHelper.RequestType.INITIAL) {
            webservice.getCharacters(
                    offset = 0,
                    limit = networkPageSize)
                    .enqueue(createWebserviceCallback(it))
        }
    }

    @MainThread
    override fun onItemAtEndLoaded(itemAtEnd: CharacterEntity) {
        helper.runIfNotRunning(PagingRequestHelper.RequestType.AFTER) {
            webservice.getCharacters(
                    offset = itemAtEnd.indexInResponse,
                    limit = networkPageSize)
                    .enqueue(createWebserviceCallback(it))
        }
    }

    private fun insertItemsIntoDb(response: Response<MarvelApi.DataWrapper<MarvelApi.CharacterResponse>>, it: PagingRequestHelper.Request.Callback) {
        ioExecutor.execute {
            handleResponse(response.body())
            it.recordSuccess()
        }
    }

    override fun onItemAtFrontLoaded(itemAtFront: CharacterEntity) {
        // ignored, since we only ever append to what's in the DB
    }

    private fun createWebserviceCallback(it: PagingRequestHelper.Request.Callback): Callback<MarvelApi.DataWrapper<MarvelApi.CharacterResponse>> {
        return object : Callback<MarvelApi.DataWrapper<MarvelApi.CharacterResponse>> {
            override fun onFailure(call: Call<MarvelApi.DataWrapper<MarvelApi.CharacterResponse>>, t: Throwable) {
                it.recordFailure(t)
            }

            override fun onResponse(call: Call<MarvelApi.DataWrapper<MarvelApi.CharacterResponse>>, response: Response<MarvelApi.DataWrapper<MarvelApi.CharacterResponse>>) {
                insertItemsIntoDb(response, it)
            }
        }
    }
}