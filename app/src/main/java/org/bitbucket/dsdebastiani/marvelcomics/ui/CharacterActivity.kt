package org.bitbucket.dsdebastiani.marvelcomics.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_characters.*
import org.bitbucket.dsdebastiani.marvelcomics.GlideApp
import org.bitbucket.dsdebastiani.marvelcomics.R
import org.bitbucket.dsdebastiani.marvelcomics.ServiceLocator
import org.bitbucket.dsdebastiani.marvelcomics.repository.NetworkState
import org.bitbucket.dsdebastiani.marvelcomics.entities.CharacterEntity

class CharacterActivity : AppCompatActivity() {

    private lateinit var model: CharacterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_characters)
        model = getViewModel()
        initAdapter()
        initSwipeToRefresh()
        (list.adapter as? CharactersAdapter)?.submitList(null)
    }

    private fun getViewModel(): CharacterViewModel {
        return ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                val repo = ServiceLocator.instance(this@CharacterActivity)
                        .getRepository()
                @Suppress("UNCHECKED_CAST")
                return CharacterViewModel(repo) as T
            }
        })[CharacterViewModel::class.java]
    }

    private fun initAdapter() {
        val glide = GlideApp.with(this)
        val adapter = CharactersAdapter(glide) {
            model.retry()
        }
        list.adapter = adapter
        model.posts.observe(this, Observer<PagedList<CharacterEntity>> {
            adapter.submitList(it)
        })
        model.networkState.observe(this, Observer {
            adapter.setNetworkState(it)
        })
    }

    private fun initSwipeToRefresh() {
        model.refreshState.observe(this, Observer {
            swipe_refresh.isRefreshing = it == NetworkState.LOADING
        })
        swipe_refresh.setOnRefreshListener {
            model.refresh()
        }
    }

}
