package org.bitbucket.dsdebastiani.marvelcomics.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.paging.LivePagedListBuilder
import android.support.annotation.MainThread
import org.bitbucket.dsdebastiani.marvelcomics.api.MarvelApi
import org.bitbucket.dsdebastiani.marvelcomics.entities.CharacterEntity
import org.bitbucket.dsdebastiani.marvelcomics.repository.db.MarvelDb
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.Executor

class DbMarvelCharacterRepository(
        val db: MarvelDb,
        private val marvelApi: MarvelApi,
        private val ioExecutor: Executor,
        private val networkPageSize: Int = DEFAULT_NETWORK_PAGE_SIZE) {
    companion object {
        private const val DEFAULT_NETWORK_PAGE_SIZE = 10
    }

    private fun insertResultIntoDb(body: MarvelApi.DataWrapper<MarvelApi.CharacterResponse>?) {
        val start = body?.data?.offset ?: 0
        body?.data?.results?.let { characters ->
            db.runInTransaction {
                val items = characters.mapIndexed { index, child ->
                    val characterEntity = CharacterEntity(
                            child.id!!,
                            child.name!!,
                            child.description,
                            child.thumbnail?.path + "." + child.thumbnail?.extension
                    )
                    characterEntity.indexInResponse = start + index
                    characterEntity
                }
                db.characters().insert(items)
            }
        }
    }

    @MainThread
    private fun refresh(): LiveData<NetworkState> {
        val networkState = MutableLiveData<NetworkState>()
        networkState.value = NetworkState.LOADING
        marvelApi.getCharacters(
                offset = 0,
                limit = networkPageSize).enqueue(
                object : Callback<MarvelApi.DataWrapper<MarvelApi.CharacterResponse>> {
                    override fun onFailure(call: Call<MarvelApi.DataWrapper<MarvelApi.CharacterResponse>>, t: Throwable) {
                        networkState.value = NetworkState.error(t.message)
                    }

                    override fun onResponse(
                            call: Call<MarvelApi.DataWrapper<MarvelApi.CharacterResponse>>,
                            response: Response<MarvelApi.DataWrapper<MarvelApi.CharacterResponse>>) {
                        ioExecutor.execute {
                            db.runInTransaction {
                                db.characters().deleteAll()
                                insertResultIntoDb(response.body())
                            }
                            networkState.postValue(NetworkState.LOADED)
                        }
                    }
                }
        )
        return networkState
    }

    @MainThread
    fun chraracters(): Listing<CharacterEntity> {
        val boundaryCallback = CharacterBoundaryCallback(
                webservice = marvelApi,
                handleResponse = this::insertResultIntoDb,
                ioExecutor = ioExecutor,
                networkPageSize = networkPageSize)
        val dataSourceFactory = db.characters().characters()
        val builder = LivePagedListBuilder(dataSourceFactory, networkPageSize).setBoundaryCallback(boundaryCallback)

        val refreshTrigger = MutableLiveData<Unit>()
        val refreshState = Transformations.switchMap(refreshTrigger) {
            refresh()
        }

        return Listing(
                pagedList = builder.build(),
                networkState = boundaryCallback.networkState,
                retry = {
                    boundaryCallback.helper.retryAllFailed()
                },
                refresh = {
                    refreshTrigger.value = null
                },
                refreshState = refreshState
        )
    }
}

