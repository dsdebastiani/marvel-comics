# Marvel Comics #

Um simples aplicativo que se comunica com a API da Marvel para listar os personagens com algumas informações.
Este projeto é totalmente experimental. Procurie utilizar concietos totalmente novos para mim. Era acostimado com arquitetura VIPER + Realm. Neste projeto resolvi mudar radicalmente para MVVM + Repository + Room + LiveData + Paging.
O prazo estipulado para o desafio era de 72horas que tive que dividir entre aprender e executar.
Utilizei a lib [https://developer.android.com/topic/libraries/architecture/paging/](Paging) para criar a lista infinita. Porém a API da Marvel não está bem estruturada para trabalhar com lista lincada, o que exigiu uma adaptação que necessita melhorias, mas está funcional.
Por fim o Layout do app ficou bem simples. Nada de mais. Mas é possível ver as informações na lsita e as imagens são carregadas de forma Lazy.

### Para que este projeto foi destinado? ###

* Este é um simples projeto de estudo no formato de desafio. Não é um app comercial.
* Versão 0.2.0

### Como compilar? ###

* Necessita do Android Studio 3.1.3 Stable
* Adicionar ao arquivo *local.properties* as chaves da [https://developer.marvel.com/account](API da Marvel):
```
marvel.api.public_key="YOUR_PUBLIC_KEY_HERE"
marvel.api.private_key="YOUR_PRIVATE_KEY_HERE"
```
* As chaves são carregadas atuomaticamentes pelo Gradle e inseridas no projeto.

### Licence ###

Copyright 2018 Daniel S. Debastiani

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
