package org.bitbucket.dsdebastiani.marvelcomics

import android.app.Application
import android.content.Context
import android.support.annotation.VisibleForTesting
import org.bitbucket.dsdebastiani.marvelcomics.api.MarvelApi
import org.bitbucket.dsdebastiani.marvelcomics.repository.DbMarvelCharacterRepository
import org.bitbucket.dsdebastiani.marvelcomics.repository.db.MarvelDb
import java.util.concurrent.Executor
import java.util.concurrent.Executors

interface ServiceLocator {

    companion object {
        private val LOCK = Any()
        private var instance: ServiceLocator? = null
        fun instance(context: Context): ServiceLocator {
            synchronized(LOCK) {
                if (instance == null) {
                    instance = DefaultServiceLocator(context.applicationContext as Application)
                }
                return instance!!
            }
        }

        @VisibleForTesting
        fun swap(locator: ServiceLocator) {
            instance = locator
        }
    }

    fun getRepository(): DbMarvelCharacterRepository

    fun getNetworkExecutor(): Executor

    fun getDiskIOExecutor(): Executor

    fun getMarvelApi(): MarvelApi
}

open class DefaultServiceLocator(val app: Application) : ServiceLocator {

    @Suppress("PrivatePropertyName")
    private val DISK_IO = Executors.newSingleThreadExecutor()

    @Suppress("PrivatePropertyName")
    private val NETWORK_IO = Executors.newFixedThreadPool(5)

    private val db by lazy {
        MarvelDb.create(app)
    }

    private val api by lazy {
        MarvelApi.create()
    }

    override fun getRepository(): DbMarvelCharacterRepository {
        return DbMarvelCharacterRepository(
                db = db,
                marvelApi = getMarvelApi(),
                ioExecutor = getDiskIOExecutor())
    }

    override fun getNetworkExecutor(): Executor = NETWORK_IO

    override fun getDiskIOExecutor(): Executor = DISK_IO

    override fun getMarvelApi(): MarvelApi = api
}