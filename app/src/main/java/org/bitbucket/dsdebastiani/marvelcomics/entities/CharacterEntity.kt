package org.bitbucket.dsdebastiani.marvelcomics.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "characters")
data class CharacterEntity(
        @PrimaryKey
        val id: Int,
        val name: String,
        val description: String?,
        val thumbnail: String?
) {
    var indexInResponse: Int = -1
}