package org.bitbucket.dsdebastiani.marvelcomics.api

import android.support.annotation.Nullable
import android.util.Log
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.bitbucket.dsdebastiani.marvelcomics.util.MarvelApiInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*

interface MarvelApi {

    // region Api Endpoints
    @GET("/v1/public/characters")
    fun getCharacters(
            @Nullable @Query("offset") offset: Int?,
            @Nullable @Query("limit") limit: Int?
    ): Call<DataWrapper<CharacterResponse>>
    // endregion

    // region Response Classes
    data class DataWrapper<T>(
            var code: Int?,
            var status: String?,
            var copyright: String?,
            var attributionText: String?,
            var attributionHTML: String?,
            var data: DataContainer<T>?,
            var etag: String?
    )

    data class DataContainer<T>(
            var offset: Int?,
            var limit: Int?,
            var total: Int?,
            var count: Int?,
            var results: List<T>
    )

    data class CharacterResponse(
            var id: Int?,
            var name: String?,
            var description: String?,
            var modified: Date?,
            var resourceURI: String?,
            var urls: List<MarvelApi.Url>?,
            var thumbnail: MarvelApi.Image?
    )

    data class Url(
            var type: String?,
            var url: String?
    )

    data class Image(
            var path: String?,
            var extension: String?
    )
    // endregion

    companion object {
        private const val BASE_URL = "https://gateway.marvel.com/"

        fun create(): MarvelApi = create(HttpUrl.parse(BASE_URL)!!)

        fun create(httpUrl: HttpUrl): MarvelApi {
            val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
                Log.d("API", it)
            })
            logger.level = HttpLoggingInterceptor.Level.BODY

            val client = OkHttpClient.Builder()
                    .addInterceptor(logger)
                    .addInterceptor(MarvelApiInterceptor())
                    .build()
            return Retrofit.Builder()
                    .baseUrl(httpUrl)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(MarvelApi::class.java)
        }

    }
}