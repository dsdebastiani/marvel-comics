package org.bitbucket.dsdebastiani.marvelcomics.util

import okhttp3.Interceptor
import okhttp3.Response
import org.bitbucket.dsdebastiani.marvelcomics.BuildConfig

class MarvelApiInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestOriginal = chain.request()
        val httpUrlOriginal = requestOriginal.url()

        val timeStamp = System.currentTimeMillis()

        val newUrl = httpUrlOriginal.newBuilder()
                .addQueryParameter("apikey", BuildConfig.MARVEL_API_PUBLIC_KEY)
                .addQueryParameter("hash", JavaUtils.MD5("$timeStamp${BuildConfig.MARVEL_API_PRIVATE_KEY}${BuildConfig.MARVEL_API_PUBLIC_KEY}"))
                .addQueryParameter("ts", "$timeStamp")
                .build()

        val rewRequest = requestOriginal.newBuilder()
                .url(newUrl)
                .build()

        return chain.proceed(rewRequest)
    }

}